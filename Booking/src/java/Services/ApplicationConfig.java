/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Andrewa
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(Booking.services.LocationsService.class);
        resources.add(Services.ActionFacadeREST.class);
        resources.add(Services.BookingFacadeREST.class);
        resources.add(Services.CategoryLocationFacadeREST.class);
        resources.add(Services.LocationFacadeREST.class);
        resources.add(Services.RoleFacadeREST.class);
        resources.add(Services.UserFacadeREST.class);
    }
    
}
