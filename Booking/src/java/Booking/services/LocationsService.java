/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Booking.services;

import Booking.models.LocationModel;
import Entities.CategoryLocation;
import Entities.Location;
import Services.LocationFacadeREST;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Andrewa
 */

@Stateless
@Path("services.locations")
public class LocationsService {
    
    LocationModel model= new LocationModel();
    List<LocationModel> modelLocations = new ArrayList<LocationModel>();
    
    
     @EJB
    private LocationFacadeREST locationFacadeREST;
    
    //TEST
    @GET
    @Consumes({"application/xml", "application/json"})
    public String sayHello() {
       return "Hello, Andrewa!!";
    }
    
    @GET 
    @Path("locations/all/{city}")
    @Produces({"application/xml", "application/json"})
    public List<LocationModel> FindAllByCity(@PathParam("city") String city) {
        
      model= new LocationModel();
      modelLocations = new ArrayList<LocationModel>();
       
       List<Location> locations = locationFacadeREST.findAll();
        for (Location location : locations) {
            if(location.getCity().toLowerCase().equals(city.toLowerCase()))
            {
                model = new LocationModel();
                model.setName(location.getName());
                model.setImagePath(location.getImagePath());
                model.setAddress(location.getAddress());
                model.setNoTables(location.getNoTables());
                
                CategoryLocation catLocation = location.getIdCategoryLocation();
                model.setCategory(catLocation.getCategory());
                
                modelLocations.add(model);  
            }
        }
       return modelLocations;
    }

    @GET 
    @Path("locations/all/{city}/{category}")
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json"})
    public List<LocationModel> FindAllByCity(@PathParam("city") String city,@PathParam("category") String category) {
        
      model= new LocationModel();
      modelLocations = new ArrayList<LocationModel>();
       
       List<Location> locations = locationFacadeREST.findAll();
        for (Location location : locations) {
            
            CategoryLocation catLocationObj = location.getIdCategoryLocation();
            String catLocation = catLocationObj.getCategory();
            
            if(location.getCity().toLowerCase().equals(city.toLowerCase()))
            {
                if(category != null && catLocation.toLowerCase().equals(category.toLowerCase()))
                {
                        model = new LocationModel();
                        model.setName(location.getName());
                        model.setImagePath(location.getImagePath());
                        model.setAddress(location.getAddress());
                        model.setNoTables(location.getNoTables());

                        modelLocations.add(model);
                }
                else
                {
                    continue;
//                model = new LocationModel();
//                model.setName(location.getName());
//                model.setImagePath(location.getImagePath());
//                model.setAddress(location.getAddress());
//                model.setNoTables(location.getNoTables());
//                
//                
//                model.setCategory(catLocation);
//                
//                modelLocations.add(model);
                }
            }
        }
       return modelLocations;
    }
}
