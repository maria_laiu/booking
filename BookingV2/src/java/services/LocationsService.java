/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import models.LocationModel;
import entities.CategoryLocation;
import entities.Location;
import entities.service.CategoryLocationFacadeREST;
import entities.service.LocationFacadeREST;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Andrewa
 */
@Stateless
@Path("services.locations")
public class LocationsService {

    LocationModel model;

    @EJB
    private LocationFacadeREST locationFacadeREST;
    
    @EJB 
    private CategoryLocationFacadeREST categoryFacadeRest;

    //TEST
    @GET
    @Consumes({"application/xml", "application/json"})
    public String sayHello() {
        return "Hello, Andrewa!!";
    }

    @GET
    @Path("all/{city}")
    @Produces({"application/xml", "application/json"})
    public List<LocationModel> FindAllByCity(@PathParam("city") String city) {

        List<LocationModel> modelLocations = FindLocations(city);
        return modelLocations;
    }

    public List<LocationModel> FindLocations(String city) {

         List<LocationModel> modelLocations = new ArrayList<LocationModel>();

        List<Location> locations = locationFacadeREST.findAll();
        for (Location location : locations) {
            if (location.getCity().toLowerCase().equals(city.toLowerCase())) {
                model =InjectFrom(location);
                modelLocations.add(model);
            }
        }
        return modelLocations;
    }

    @GET
    @Path("count/{city}")
    @Produces(MediaType.TEXT_PLAIN)
    public int CountByCity(@PathParam("city") String city) {

        List<LocationModel> modelLocations = FindLocations(city);
        int count = modelLocations.size();
        return count;
    }
    
    @GET
    @Path("{city}/{name}")
    @Produces({"application/xml", "application/json"})
    public List<LocationModel> FindAllByName(@PathParam("city") String city,@PathParam("name") String name){

       List<LocationModel> modelLocations = new ArrayList<LocationModel>();
       List<LocationModel> locations = FindLocations(city);

        for (LocationModel location : locations) {
                if (location.getName().toLowerCase().startsWith(name.toLowerCase()) ||location.getName().toLowerCase().contains(name.toLowerCase()) ) {
                    modelLocations.add(location);
                }
        }
        return modelLocations;
    }
    
    @GET
    @Path("countByName/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public int CountByName(@PathParam("name") String name){

       List<LocationModel> modelLocations =FindAllByName("iasi",name);
       int count = modelLocations.size();
       return  count;
    }
    
    @GET
    @Path("getLocationById/{id}")
    @Produces({"application/xml", "application/json"})
    public LocationModel GetLocationById(@PathParam("id") int id){

       Location location =locationFacadeREST.find(id);
       model = InjectFrom(location);
       return  model;
    }
    
    @POST
    @Path("add")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void addLocation (LocationModel locationModel){
        Location location = new Location();
        CategoryLocation category = categoryFacadeRest.findCategoryByName(locationModel.getCategory());
        location.setIdCategoryLocation(category); 
        location.setName(locationModel.getName());
        location.setCity(locationModel.getCity());
        location.setAddress(locationModel.getAddress());
        location.setImagePath(locationModel.getImagePath());
        location.setLatitude(locationModel.getLatitude());
        location.setLongitude(locationModel.getLongitude());
        location.setNoTables(locationModel.getNoTables());
        location.setTelephone(locationModel.getTelephone());
        locationFacadeREST.create(location);
    }
    
    @POST
    @Path("edit")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void editLocation(LocationModel locationModel){
        Location location = new Location();
        CategoryLocation category = categoryFacadeRest.findCategoryByName(locationModel.getCategory());
        location.setId(locationModel.getId());
        location.setIdCategoryLocation(category); 
        location.setName(locationModel.getName());
        location.setCity(locationModel.getCity());
        location.setAddress(locationModel.getAddress());
        location.setImagePath(locationModel.getImagePath());
        location.setLatitude(locationModel.getLatitude());
        location.setLongitude(locationModel.getLongitude());
        location.setNoTables(locationModel.getNoTables());
        location.setTelephone(locationModel.getTelephone());
        locationFacadeREST.edit(location);
    }
    
    public LocationModel InjectFrom(Location location)
    {
        model = new LocationModel();

        CategoryLocation catLocationObj = location.getIdCategoryLocation();
        String category = catLocationObj.getCategory();
        
        model.setId(location.getId());
        model.setName(location.getName());
        model.setAddress(location.getAddress());
        String tel = location.getTelephone();
        model.setTelephone(tel);
        model.setCategory(category);
        model.setCity(location.getCity());
        model.setImagePath(location.getImagePath());
        model.setLatitude(location.getLatitude());
        model.setLongitude(location.getLongitude());
        model.setNoTables(location.getNoTables());
        
        return model; 
    }
}
