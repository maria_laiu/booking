/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Action;
import entities.service.ActionFacadeREST;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import models.ActionModel;

/**
 *
 * @author mlaiu
 */
@Stateless
@Path("service.actions")
public class ActionService {
    
    @EJB
    private ActionFacadeREST actionFacadeREST;
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<ActionModel> findAllActions(){
        List<ActionModel> actionsResult = new ArrayList<ActionModel>() ;
        List<Action> actionsFromDB = actionFacadeREST.findAll();
        for(Action action : actionsFromDB){
            actionsResult.add(fillActionModelFromActionEntity(action));
        }
        return actionsResult;
    }
    
    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public ActionModel getActionById(@PathParam("id") Integer id){
        Action action = actionFacadeREST.find(id);
        return fillActionModelFromActionEntity(action);
    }
        
    public ActionModel fillActionModelFromActionEntity(Action action){
        ActionModel actionModel = new ActionModel();
        actionModel.setId(action.getId());
        actionModel.setActionName(action.getActionName());
        actionModel.setActionCode(action.getActionCode());
        return actionModel;
    }
    
}
