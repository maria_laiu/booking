/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Booking;
import entities.Location;
import entities.service.BookingFacadeREST;
import entities.service.LocationFacadeREST;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import models.BookingModel;
import models.LocationModel;

/**
 *
 * @author Andrewa
 */
@Stateless
@Path("services.booking")
public class BookingService {
    
    @EJB
    private BookingFacadeREST bookingFacadeREST;
    @EJB
    private LocationFacadeREST locationFacadeREST;
    
    //TEST
    @GET
    @Consumes({"application/xml", "application/json"})
    public String sayHello() {
        return "Hello, Andrewa!!";
    }
    
    @POST
    @Path("saveNewBooking")
    @Consumes({"application/xml", "application/json"})
    public int SaveBooking(BookingModel newBooking) throws ParseException {

        Booking booking = new Booking();
        booking.setFirstName(newBooking.getFirstName());
        booking.setLastName(newBooking.getLastName());
        booking.setEmail(newBooking.getEmail());
        booking.setTelephone(newBooking.getTelephone());
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse(newBooking.getBookedOnString());
        
        booking.setDate(date);
        booking.setFromHour(newBooking.getFromHour() + ":00");
        String untilHour=newBooking.getUntilHour() + ":00";
        if(newBooking.getUntilHour() == null)
        {
            int hours = Integer.parseInt(newBooking.getFromHour()) + 3;
            untilHour = hours + ":00";
        }
        booking.setUntilHour(untilHour);
        booking.setComment(newBooking.getComment());
        
        
        int locationId = newBooking.getLocationId();
        Location location = locationFacadeREST.find(locationId);
        
        booking.setIdLocation(location);
        booking.setNoPersons(newBooking.getNo_persons());
        booking.setNoTables(newBooking.getNo_persons()/4);
        booking.setCreatedOn(new Date());
        booking.setStatus("CREATED");
        
        bookingFacadeREST.create(booking);
        return 1; 
    }
}
