/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Action;
import entities.Role;
import entities.User;
import entities.service.RoleFacadeREST;
import entities.service.UserFacadeREST;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import models.ActionModel;
import models.RoleModel;

/**
 *
 * @author mlaiu
 */
@Stateless
@Path("services.role")
public class RoleService {
    
    @EJB
    private RoleFacadeREST roleFacadeRest;
    
    @EJB
    private ActionService actionService;
    
    @EJB
    private RoleService roleService;
    
    @EJB
    private UserFacadeREST userFacadeRest;
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<RoleModel> getAllRoles(){
        List<RoleModel> result = new ArrayList<RoleModel>();
        List<Role> roles = roleFacadeRest.findAll();
        for(Role role : roles){
            result.add(fillRoleModelFromRoleEntity(role));
        }
        return result;
    }
    
    @GET
    @Path("/{role}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public RoleModel findRoleByName(@PathParam("role")String roleName){
        RoleModel roleModel = new RoleModel();
        Role role = roleFacadeRest.findRoleByName(roleName);
        return fillRoleModelFromRoleEntity(role);
    }
    
    @GET
    @Path("roles/{username}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public RoleModel getRoleByUser(@PathParam("username") String username){
        User user = userFacadeRest.findUserByUsername(username);
        List<Role> roles = (List<Role>) user.getRoleCollection();
        Role firstRole = roles.get(0);
        return roleService.fillRoleModelFromRoleEntity(firstRole);
    }
    
    public RoleModel fillRoleModelFromRoleEntity(Role role){
        RoleModel roleModel = new RoleModel();
        List<Action> actionsFromRome = (List<Action>) role.getActionCollection();
        List<ActionModel> actionModelList = new ArrayList<ActionModel>();
        roleModel.setId(role.getId());
        roleModel.setName(role.getName());
        for(Action action : actionsFromRome){
            actionModelList.add(actionService.fillActionModelFromActionEntity(action));
        };
        return roleModel;
    }
}
