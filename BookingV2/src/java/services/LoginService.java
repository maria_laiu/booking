/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import models.UserWrapper;
import utils.Md5Util;

/**
 *
 * @author mlaiu
 */
@Stateless
@Path("/login")
public class LoginService {
       
    @EJB
    UserService userService;
    
    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String authenticateUser (UserWrapper userWrapper) throws Exception{
        String username = userWrapper.getUsername();
        String password = userWrapper.getPassword();
        
        String runtimeToken = Md5Util.generateMd5(username.concat(password));
        String userTokenFromDB = userService.getTokenByUsername(username);
        if(userTokenFromDB != null){
            if(runtimeToken.equals(userTokenFromDB)){
                return createResponseObject("Success", true, runtimeToken);
            }
            else{
                return createResponseObject("Incorrect password", false, "");
            }
        }
        
        return createResponseObject("Incorrect username", false, "");
    }
    
    private String createResponseObject(String message, boolean success, String token) {
        JsonBuilderFactory factory = Json.createBuilderFactory(null);
        JsonObject response = factory.createObjectBuilder()
            .add("message", message)
            .add("success", success)
            .add("token", token)
            .build();
        
        return response.toString();
        
    }
    
}
