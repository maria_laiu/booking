/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Location;
import entities.Role;
import entities.User;
import entities.service.LocationFacadeREST;
import entities.service.RoleFacadeREST;
import entities.service.UserFacadeREST;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import models.AddUserModel;
import models.LocationModel;
import models.RoleModel;
import models.UserModel;
import models.UserWrapper;
import utils.Md5Util;

/**
 *
 * @author mlaiu
 */
@Stateless
@Path("services.users")
public class UserService {
    
    @EJB
    private UserFacadeREST userFacadeRest;
    
    @EJB
    private RoleService roleService;
    
    @EJB
    private RoleFacadeREST roleFacadeRest;
    
    @EJB 
    private LocationsService locationService;
    
    @EJB 
    private LocationFacadeREST locationFacadeRest;
    
    @GET
    public String test(){
        return "Test!";
    }
    
    @GET
    @Path("token/{username}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String getTokenByUsername(@PathParam("username") String username){
        List<User> users = userFacadeRest.findAll(); 
        for(User user: users){
            if(user.getUsername().equals(username)){
                return user.getToken();
            }
        }
        return null;
    }

    @POST
    @Path("changePassword")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void changePassword(UserWrapper userWrapper) throws Exception{
        String username = userWrapper.getUsername();
        String password = userWrapper.getPassword();
        User user = userFacadeRest.findUserByUsername(username);
        user.setToken(Md5Util.generateMd5(username.concat(password)));
        userFacadeRest.edit(user);
    }
    
    @GET
    @Path("users/{username}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public UserModel findByUsername(@PathParam("username") String username){
       User user = userFacadeRest.findUserByUsername(username);
       return fillUserModelFromUserEntity(user);
    }    
    
    @POST
    @Path("/add")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void addUser(AddUserModel userModel) throws Exception{
        User user = new User();
        Role role = roleFacadeRest.findRoleByName(userModel.getRole());
        Collection<Role> roles = new ArrayList<Role>();
        roles.add(role);
        
        Location location = null;
        if(userModel.getLocation() != null){
           location = locationFacadeRest.find(userModel.getLocation().getId());
        }
        
        user.setUsername(userModel.getUsername());
        user.setToken(Md5Util.generateMd5(userModel.getUsername().concat(userModel.getPassword())));
        user.setFirstName(userModel.getFirstName());
        user.setLastName(userModel.getLastName());
        user.setEmail(userModel.getEmail());
        user.setActive(true);
        user.setRoleCollection(roles);
        user.setIdLocation(location);
        userFacadeRest.create(user);
    }
    
    @POST
    @Path("edit")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void editUser(AddUserModel userModel) throws Exception{
        User user = new User();
        user.setId(userModel.getId());
        Role role = roleFacadeRest.findRoleByName(userModel.getRole());
        Collection<Role> roles = new ArrayList<Role>();
        roles.add(role);
        
        Location location = null;
        if(userModel.getLocation() != null){
           location = locationFacadeRest.find(userModel.getLocation().getId());
        }        
        
        user.setUsername(userModel.getUsername());
        String token = null;
        if(userModel.getPassword()!= null){
            token = Md5Util.generateMd5(userModel.getUsername().concat(userModel.getPassword()));
        }
        else{
            User userFromDb = userFacadeRest.find(userModel.getId());
            token = userFromDb.getToken();
        }
        user.setToken(token);
   
        user.setFirstName(userModel.getFirstName());
        user.setLastName(userModel.getLastName());
        user.setEmail(userModel.getEmail());
        user.setActive(true);
        user.setRoleCollection(roles);
        user.setIdLocation(location);
        userFacadeRest.edit(user);
    }
      
    public UserModel fillUserModelFromUserEntity(User user){
        UserModel userModel = new UserModel();
        List<Role> roles = (List<Role>) user.getRoleCollection();
        List<RoleModel> roleModelList = new ArrayList<RoleModel>();
        Location location = user.getIdLocation();
        LocationModel locationModel = locationService.InjectFrom(location);
        
        userModel.setId(user.getId());
        userModel.setUsername(user.getUsername());
        userModel.setToken(user.getToken());
        userModel.setEmail(user.getEmail());
        userModel.setFirstName(user.getFirstName());
        userModel.setLastName(user.getLastName());
        userModel.setActive(user.getActive());   
        for(Role role: roles){
            roleModelList.add(roleService.fillRoleModelFromRoleEntity(role));
        }
        userModel.setRoles(roleModelList);
        userModel.setLocation(new LocationModel());
        return userModel;
    }
        
}
