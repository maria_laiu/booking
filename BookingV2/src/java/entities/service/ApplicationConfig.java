/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Alex C
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(entities.service.ActionFacadeREST.class);
        resources.add(entities.service.BookingFacadeREST.class);
        resources.add(entities.service.CategoryLocationFacadeREST.class);
        resources.add(entities.service.LocationFacadeREST.class);
        resources.add(entities.service.RoleFacadeREST.class);
        resources.add(entities.service.UserFacadeREST.class);
        resources.add(services.ActionService.class);
        resources.add(services.BookingService.class);
        resources.add(services.LocationsService.class);
        resources.add(services.LoginService.class);
        resources.add(services.RoleService.class);
        resources.add(services.UserService.class);
    }
    
}
