/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author mlaiu
 */
public class BookingModel {
    
    private String id;
    private Date date;
    private int no_persons;
    private int no_tables;
    private String firstName;
    private String lastName;
    private String fromHour;
    private String untilHour;
    private String email;
    private int telephone;
    private String status;
    private Date createdOn;
    private Date bookedOn;
    private String bookedOnString;
    private String comment;
    private int locationId;
    private LocationModel location;

    public BookingModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNo_persons() {
        return no_persons;
    }

    public void setNo_persons(int no_persons) {
        this.no_persons = no_persons;
    }

    public int getNo_tables() {
        return no_tables;
    }

    public void setNo_tables(int no_tables) {
        this.no_tables = no_tables;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getUntilHour() {
        return untilHour;
    }

    public void setUntilHour(String untilHour) {
        this.untilHour = untilHour;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocationModel getLocation() {
        return location;
    }

    public void setLocation(LocationModel location) {
        this.location = location;
    }   
    
     public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }   
    
    public Date getBookedOn() {
        return bookedOn;
    }

    public void setBookedOn(Date bookedOn) {
        this.bookedOn = bookedOn;
    }  
    
    public String getBookedOnString() {
        return bookedOnString;
    }

    public void setBookedOnString(String bookedOnString) {
        this.bookedOnString = bookedOnString;
    }  
    
}
