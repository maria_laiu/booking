/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;
/**
 *
 * @author mlaiu
 */
public class ActionModel {
    
    private Integer id;
    private String actionName;
    private String actionCode;
    
    public ActionModel() {
    }

    public ActionModel(Integer id) {
        this.id = id;
    }

    public ActionModel(Integer id, String actionName) {
        this.id = id;
        this.actionName = actionName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }
   
}
