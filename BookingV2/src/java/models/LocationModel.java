/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

/**
 *
 * @author Andrewa
 */
public class LocationModel {
    private Integer id;
    private String name;
    private String city;
    private String address;
    private String telephone;
    private Double latitude;
    private Double longitude;
    private int noTables;
    private String imagePath;
    private String category;
    private List<UserModel> users;

    public LocationModel() {
    }

    public LocationModel(Integer id) {
        this.id = id;
    }

    public LocationModel(Integer id, String name, String city, String address, int noTables) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.address = address;
        this.noTables = noTables;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public int getNoTables() {
        return noTables;
    }

    public void setNoTables(int noTables) {
        this.noTables = noTables;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }
    
    
}
