/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

/**
 *
 * @author mlaiu
 */
public class RoleModel {
    
    private Integer id;
    private String name;
    private List<ActionModel> actions;

    public RoleModel() {
    }

    public RoleModel(Integer id) {
        this.id = id;
    }

    public RoleModel(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ActionModel> getActions() {
        return actions;
    }

    public void setActions(List<ActionModel> actionCollection) {
        this.actions = actionCollection;
    }
    
}
