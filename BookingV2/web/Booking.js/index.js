var app = angular.module('app', ['angularUtils.directives.dirPagination']);

app.controller('MainController', function ($scope, $http) {

    $scope.currentPage = 1;
    $scope.pageSize = 2;
    $scope.restaurants = [];

    GetAllRestaurants($scope, $http);


    $scope.getRestaurantsByName = function () {
        debugger;

        var searchFor = $("#input-search").val();
        if (searchFor === undefined || searchFor === "")
        {
            GetAllRestaurants($scope, $http);
        }

        //count restaurants
        $http.get('webresources/services.locations/countByName/' + searchFor)
                .success(function (data) {
                    $scope.restCount = data;
                })
                .error(function () {
                    console.log('Error services.locations/count/iasi');
                });



        //all restaurants by name
        $http.get('webresources/services.locations/iasi/' + searchFor)
                .success(function (data) {
                    $scope.locations = data;
                })
                .error(function () {
                    console.log('Error services.locations/iasi/name ');
                });
    }
});

function GoToRestaurantPage(myThis)
{
    var restaurantId = $(myThis).attr("restaurant-id");
    window.location = "Booking.views/restaurant.html?rstId=" + restaurantId;
}

function GetAllRestaurants($scope, $http)
{
    //all restaurants
    $http.get('webresources/services.locations/all/iasi')
            .success(function (data) {

                for (var loc in data)
                {
                    if (data[loc].imagePath === null || data[loc].imagePath === undefined || data[loc].imagePath === "")
                    {
                        data[loc].imagePath = "default-restaurant.gif";
                    }
                }
                $scope.locations = data;
            })
            .error(function () {
                console.log('Error services.locations/all/iasi ');
            });

    //count restaurants
    $http.get('webresources/services.locations/count/iasi')
            .success(function (data) {
                $scope.restCount = data;
            })
            .error(function () {
                console.log('Error services.locations/count/iasi');
            });
}


function OtherController($scope) {
    $scope.pageChangeHandler = function (num) {
        console.log('going to page ' + num);
    };
}

app.controller('OtherController', OtherController);




