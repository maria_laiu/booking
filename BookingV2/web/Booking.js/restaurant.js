

var app = angular.module('app', []);

app.controller('MainController', function ($scope, $http) {

    var location = window.location.href;
    var index = location.indexOf("=");
    var restaurantId = location.substr(index + 1, 1);

    //Restaurants info - picture, name, address, program
    $http.get('../webresources/services.locations/getLocationById/' + restaurantId)
            .success(function (data) {
                $scope.restaurantInfo = data;
                var lat = data.latitude;
                var long = data.longitude;
                initMap(lat, long);
            })
            .error(function () {
                console.log('Error services.restaurants');
            });

    //Fill dropdowns From
    $http.get('')
            .success(function (data) {
                $scope.dataDropdowns = data;
            })
            .error(function () {
                console.log('Error services.restaurants');
            });

    $scope.SaveBooking = function (booking)
    {
        var ok = validareDate(booking);
        debugger;
        if(ok ===true)
        {
            booking.bookedOnString = $('#datepicker').val();
            booking.no_persons = $('#repeatPersonNo').val().substring(2,3);
            booking.locationId = restaurantId;
            $http({url: '../webresources/services.booking/saveNewBooking',
                method: 'POST',
                headers: {"Content-Type":"application/json"},
                data: JSON.stringify(booking)
                }).then(function () {
                    $("#reservationMsg").css("display",'block');
                    $("#reservationDiv").css("display",'none');
                    $("#reservationMap").css("display",'none');
                    
                }, function (error) {
                    console.log(error);
                });
             document.getElementById('lblMessage').style.display = 'block';
                 }  
                
            };
});

function initMap(lat, long) {
    var map = new google.maps.Map(document.getElementById('googleMap'), {
        zoom: 17,
        center: {lat: lat, lng: long}
    });

    var marker = new google.maps.Marker({
        position: map.getCenter(),
        icon: {
            path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
            scale: 4,
            strokeWeight: 2,
            strokeColor: "#B40404"
        },
        draggable: true,
        map: map
    });

}

function validareDate(array) {
    var ok = 0;

//    if (array === undefined)
//    {
        $("#inputFirstName").css("backgroundColor", "#EB9C9C");
        $("#inputLastName").css("backgroundColor", "#EB9C9C");
        $("#inputEmail").css("backgroundColor", "#EB9C9C");
        $("#inputPhone").css("backgroundColor", "#EB9C9C");
        $("#datepicker").css("backgroundColor", "#EB9C9C");
        $("#repeatSelectFrom").css("backgroundColor", "#EB9C9C");
        $("#repeatPersonNo").css("backgroundColor", "#EB9C9C");
       // ok = 1;
//    }
//    else
//    {
        if(array !== undefined)
        {
            if (array.firstName !== undefined) {
            $("#inputFirstName").css("backgroundColor", "white");
            ok = 1;
        }
        if (array.lastName !== undefined) {
            $("#inputLastName").css("backgroundColor", "white");
            ok = 1;
        }
        if (array.email !== undefined) {
            $("#inputEmail").css("backgroundColor", "white");
            ok = 1;
        }
        if (array.telephone !== undefined) {
            $("#inputPhone").css("backgroundColor", "white");
            ok = 1;
        }
        
        
        
        if (array.selectedDate !== undefined ) {
            
            var myDate = new Date(array.selectedDate);
            var thisDate = new Date();
            if( myDate > thisDate)
            {
                   $("#datepicker").css("backgroundColor", "white");
                    ok = 1;
            }
         
        }

        if (array.fromHour !== undefined) {
            $("#repeatSelectFrom").css("backgroundColor", "white");
            ok = 1;
        }
        
        if (array.no_persons !== undefined) {
            $("#repeatPersonNo").css("backgroundColor", "white");
            ok = 1;
        }
        }
        
    //}
        
     if(ok===1) return true;   
    return false;
};

function lettersOnly(event)
{
     var inputValue = event.which;
        // allow letters and whitespaces only.
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
            event.preventDefault(); 
        }
  }
  
 function isNumberKey(event) {
            var charCode = (event.which) ? event.which : event.keyCode
            if ((charCode < 48 || charCode > 57))
                return false;
 
            return true;
}

