'use strict';

angular.module('Administrator')

.controller('AdministratorController',
	['$scope', '$http',
	function ($scope, $http) {

		/***************************
		* LOCATIONS FUNCTIONALITIES
		****************************/
		$scope.imageBase = '../Booking.images';
		$scope.show = "main";

		$scope.getRestaurants = function() {
			var endpoint = "http://localhost:8080/BookingV2/webresources/services.locations/all/Iasi";

			$http.get(endpoint, {}).then(function(response, status) {
				$scope.restaurants = response.data;
			}).catch(function(response){
				$.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
				console.log("error" + data.responseText +"  " + data.status);   
			});
		}

		$scope.getCategories = function() {
			var endpoint = "http://localhost:8080/BookingV2/webresources/entities.categorylocation";

			$http.get(endpoint, {}).then(function(response, status) {
				$scope.categories = response.data;
			}).catch(function(response){
				$.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
				console.log("error" + data.responseText +"  " + data.status);   
			});

			console.log($scope.categories);
		}

		$scope.toggleEditLocation = function(name, address, city, category, latitude, longitude, noTables, telephone, id) {
			$scope.show = 'edit';
			$scope.action = 'edit';

			$scope.newLocationName = name;
			$scope.newLocationAddress = address;
			$scope.newLocationCity = city
			$scope.newLocationCategory = category
			$scope.newLocationLatitude = latitude;
			$scope.newLocationLongitude = longitude;
			$scope.newLocationNoTables = noTables;
			$scope.newLocationTelephone = telephone;
			$scope.newLocationId = id;

			console.log(category);
		}

		$scope.toggleCreateLocation = function() {
			$scope.show = 'edit';
			$scope.action= 'create';

			$scope.newLocationName = '';
			$scope.newLocationAddress = '';
			$scope.newLocationCity = '';
			$scope.newLocationCategory = '';
			$scope.newLocationLatitude = '';
			$scope.newLocationLongitude = '';
			$scope.newLocationNoTables = '';
			$scope.newLocationTelephone = '';
			$scope.newLocationId = '';
		}

		$scope.resetShow = function() {
			$scope.show = 'main';
			setTimeout(function(){ location.reload(); }, 500);
		}

		$scope.editLocation = function() {
			var endpoint = "http://localhost:8080/BookingV2/webresources/services.locations/edit";

			var data = {
				"id": $scope.newLocationId,
				"address":$scope.newLocationAddress,
				"category":$scope.newLocationCategory,
				"city":$scope.newLocationCity,
				"latitude":$scope.newLocationLatitude,
				"longitude":$scope.newLocationLongitude,
				"name":$scope.newLocationName,
				"noTables":$scope.newLocationNoTables,
				"telephone":$scope.newLocationTelephone};

			$http.post(endpoint, data).then(function(response, status) {
              	$.notifyBar({ cssClass: "success", html: "Location edited with success" }); 
                console.log("error" + data.responseText +"  " +data.status);   

             }).catch(function(response){
                $.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
                console.log("error" + data.responseText +"  " +data.status);   
             });
		}

		$scope.createNewLocation = function() {
			var endpoint = "http://localhost:8080/BookingV2/webresources/services.locations/add";

			var data = {
				"address":$scope.newLocationAddress,
				"category":$scope.newLocationCategory,
				"city":$scope.newLocationCity,
				"latitude":$scope.newLocationLatitude,
				"longitude":$scope.newLocationLongitude,
				"name":$scope.newLocationName,
				"noTables":$scope.newLocationNoTables,
				"telephone":$scope.newLocationTelephone};

			$http.post(endpoint, data).then(function(response, status) {
              	$.notifyBar({ cssClass: "success", html: "New location added with success" }); 
                console.log("error" + data.responseText +"  " +data.status);   

             }).catch(function(response){
                $.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
                console.log("error" + data.responseText +"  " +data.status);   
             });
		}

		$scope.getRestaurants();
		$scope.getCategories();

		/***************************
		* LOCATIONS FUNCTIONALITIES - END
		****************************/

		/***************************
		* USERS FUNCTIONALITIES
		****************************/

		$scope.getUsers = function() {
			var endpoint = "http://localhost:8080/BookingV2/webresources/entities.user";

			$http.get(endpoint, {}).then(function(response, status) {
				$scope.users = response.data;
			}).catch(function(response){
				$.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
				console.log("error" + data.responseText +"  " + data.status);   
			});
		}


		$scope.toggleEditUser = function(email,username, isActive, firstname, lastname, userlocation, password, role, id, token) {
			$scope.show = 'edit';
			$scope.action = 'edit';

			$scope.newEmail = email;
			$scope.newUserName = username;
			$scope.newActiveState = isActive;
			$scope.newFirstName = firstname;
			$scope.newLastName = lastname;
			$scope.newUserLocation = userlocation;
			$scope.newPassword = password;
			$scope.newRole = role;
			$scope.newUserId = id;
			$scope.token = token;

			console.log($scope.newUserId);

		}

		$scope.toggleCreateUser = function() {
			$scope.show = 'edit';
			$scope.action= 'create';

			$scope.newEmail = '';
			$scope.newUserName = '';
			$scope.newActiveState = '';
			$scope.newFirstName = '';
			$scope.newLastName = '';
			$scope.newUserLocation = '';
			$scope.newPassword = '';
			$scope.newRole = '';
			$scope.newUserId = '';
			$scope.token = '';
		}

		$scope.editUser = function() {
			var endpoint = "http://localhost:8080/BookingV2/webresources/services.users/edit";

			var data = {
				"active":$scope.newActiveState,
				"email":$scope.newEmail,
				"firstName":$scope.newFirstName,
				"id":$scope.newUserId,
				"lastName":$scope.newLastName,
				"location":{"noTables":0},
				"roles":[],
				"password":$scope.newPassword,
				"username":$scope.newUserName,
				"location":{
					"address":"Anastasie Panu Street",
					"category":"Italian",
					"city":"Iasi"
					,"id":$scope.newUserLocation,
					"latitude":47.1603763,
					"longitude":27.59056899999996,
					"name":"Credo",
					"noTables":14,
					"telephone":"740663364"
				}
			}

			$http.post(endpoint, data).then(function(response, status) {
              	$.notifyBar({ cssClass: "success", html: "User was edited with success" }); 
                console.log("error" + data.responseText +"  " +data.status);   

             }).catch(function(response){
                $.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
                console.log("error" + data.responseText +"  " +data.status);   
             });
		}

		$scope.createNewUser = function() {
			var endpoint = "http://localhost:8080/BookingV2/webresources/services.users/add";

			var data = {
				"email":$scope.newEmail,
				"firstName":$scope.newFirstName,
				"lastName":$scope.newLastName,
				"role":$scope.newRole,
				"password":$scope.newPassword,
				"username":$scope.newUserName, 
				"location":{
					"address":"Anastasie Panu Street",
					"category":"Italian",
					"city":"Iasi",
					"id":$scope.newUserLocation,
					"latitude":47.1603763,
					"longitude":27.59056899999996,
					"name":"Credo",
					"noTables":14,
					"telephone":"740663364"
				}
			}

			$http.post(endpoint, data).then(function(response, status) {
              	$.notifyBar({ cssClass: "success", html: "New user was added with success" }); 
                console.log("error" + data.responseText +"  " +data.status);   

             }).catch(function(response){
                $.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
                console.log("error" + data.responseText +"  " +data.status);   
             });
		}

		$scope.getUsers();
		/***************************
		* USERS FUNCTIONALITIES END
		****************************/

		console.log("admin controller called.")
	}]);