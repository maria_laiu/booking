'use strict';

angular.module('internalApp').controller('AdminController', ['$scope', '$http', function($scope, $http){
    $scope.title = 'admin';
}]);

angular.module('Authentication')

.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService', '$http',
    function ($scope, $rootScope, $location, AuthenticationService, $http) {

        function routeByRole(username) {
            var endpoint = "http://localhost:8080/BookingV2/webresources/services.role/roles/" + username;

            $http.get(endpoint, {}).then(function(response, status) {
                $scope.role = response.data.name;

                route($scope.role);

            }).catch(function(response){
                $.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
                console.log("error" + data.responseText +"  " +data.status);   
            });
        }

        // need to change this to use roles
        function route(name) {
            switch(name) {
                case 'ADMIN':
                $location.path("/administrator/users");
                break;
                case 'MANAGER':
                $location.path("/manager/waiters");
                break;
                case 'WAITER':
                $location.path("/waiter/tables");
                break;
                default:
                $location.path('/login');
            }
        }

        // reset login status
        AuthenticationService.ClearCredentials();

        $scope.login = function () {

            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function(response) {
                if(response.success) {
                    AuthenticationService.SetCredentials($scope.username, response.token);
                    
                    routeByRole($scope.username);
                    //$location.path('/');
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        };
    }]);