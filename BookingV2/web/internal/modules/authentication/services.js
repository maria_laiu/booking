'use strict';
 
angular.module('Authentication')
 
.factory('AuthenticationService',
    ['$http', '$cookieStore', '$rootScope', '$timeout',
    function ($http, $cookieStore, $rootScope, $timeout) {
        var service = {};
        var endpoint = "http://localhost:8080/BookingV2/webresources/login";
        $http.defaults.headers.common.Accept = "application/json";

        service.Login = function (username, password, callback) {

             var data = "{\"username\":\"" + username + "\",\"password\":\""+ password +"\"}";

             $http.post(endpoint, data).then(function(response, status) {
                callback(response.data)
             }).catch(function(response){
                $.notifyBar({ cssClass: "error", html: "A server error has occured! Please try again later... :(" }); 
                console.log("error" + data.responseText +"  " +data.status);   
             });
        };
 
        service.SetCredentials = function (username, authdata) {
 
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };
 
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        };
 
        service.ClearCredentials = function () {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic ';
        };
 
        return service;
    }])