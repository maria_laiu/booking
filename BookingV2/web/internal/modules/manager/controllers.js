'use strict';
 
angular.module('Manager')
 
.controller('ManagerController',
    ['$scope', '$routeParams',
    function ($scope, $routeParams) {
     	$scope.title = 'manager'; 

     	$scope.testMethod = function() {
 			return 'what? I am manager';
     	}
	}]);