'use strict';

// declare modules
angular.module('Authentication', []);
angular.module('Administrator', []);
angular.module('Manager', []);
angular.module('Waiter', []);

angular.module('internalApp', [
    'Authentication',
    'Administrator',
    'Manager',
    'Waiter',
    'ngRoute',
    'ngCookies'
    ])

.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
    .when('/login', {
        controller: 'LoginController',
        templateUrl: 'modules/authentication/views/login.html'
    })

    .when('/administrator/:view', {
        controller: 'AdministratorController',
        templateUrl: function(params) {
            console.log(params);
            return 'modules/administrator/views/' + params.view +'.html';
        }    
    })

    .when('/manager/:view', {
        controller: 'ManagerController',
        templateUrl: function(params) {
            console.log(params);
            return 'modules/manager/views/' + params.view +'.html';
        }   
    })

    .when('/waiter/:view', {
        controller: 'WaiterController',
        templateUrl: function(params) {
            console.log(params);
            return 'modules/waiter/views/' + params.view +'.html';
        }    
    })

    .otherwise({ redirectTo: '/login' });
}])

.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        var currentUser = $cookieStore.get('globals') || {};

        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);