-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25 Mai 2016 la 16:42
-- Versiune server: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `booking`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `action`
--

CREATE TABLE `action` (
  `id` int(50) NOT NULL,
  `actionName` varchar(100) NOT NULL,
  `actionCode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `action`
--

INSERT INTO `action` (`id`, `actionName`, `actionCode`) VALUES
(1, 'Add restaurant', NULL),
(2, 'Modify restaurant', NULL),
(3, 'Delete restaurant', NULL),
(4, 'Add user', NULL),
(5, 'Modify user', NULL),
(6, 'Delete user', NULL),
(7, 'Add waiter', NULL),
(8, 'Modify waiter', NULL),
(9, 'Delete waiter', NULL),
(10, 'Add order', NULL),
(11, 'Change order', NULL),
(12, 'Delete order', NULL),
(13, 'Change booking', NULL),
(14, 'Delete booking', NULL),
(15, 'Add booking', NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `booking`
--

CREATE TABLE `booking` (
  `id` int(50) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `id_location` int(11) NOT NULL,
  `date` date NOT NULL,
  `no_persons` int(10) NOT NULL,
  `no_tables` int(10) DEFAULT NULL,
  `fromHour` varchar(5) NOT NULL,
  `untilHour` varchar(5) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telephone` int(50) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdOn` date NOT NULL,
  `lastModifiedBy` int(50) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `booking`
--

INSERT INTO `booking` (`id`, `firstName`, `lastName`, `id_location`, `date`, `no_persons`, `no_tables`, `fromHour`, `untilHour`, `email`, `telephone`, `status`, `createdOn`, `lastModifiedBy`, `comment`) VALUES
(1, 'Chris', 'Martin', 3, '2016-05-21', 0, NULL, '10:00', '13:00', 'maria.laiu92@gmail.com', 0, 'CREATED', '2016-05-19', 1, NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `category_location`
--

CREATE TABLE `category_location` (
  `id` int(50) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `category_location`
--

INSERT INTO `category_location` (`id`, `category`) VALUES
(1, 'Italian'),
(2, 'Chineze'),
(3, 'Tradional'),
(5, 'Select');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `location`
--

CREATE TABLE `location` (
  `id` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `telephone` int(20) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `id_category_location` int(50) NOT NULL,
  `no_tables` int(50) NOT NULL,
  `image_path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `location`
--

INSERT INTO `location` (`id`, `name`, `city`, `address`, `telephone`, `latitude`, `longitude`, `id_category_location`, `no_tables`, `image_path`) VALUES
(1, 'Buena Vista', 'Iasi', '43, Petru Movila Street', 0, NULL, NULL, 5, 12, ''),
(2, 'Credo', 'Iasi', 'Anastasie Panu Street', 0, NULL, NULL, 1, 14, NULL),
(3, 'Doppio zero', 'Iasi', 'Tudor Vladimirescu, 24', 0, NULL, NULL, 1, 18, NULL),
(4, 'Chef Galerie', 'Iasi', 'Palace street, no 3', 0, NULL, NULL, 5, 21, NULL),
(5, 'Cao', 'Iasi', 'Smardan street', 0, NULL, NULL, 2, 26, NULL);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `role`
--

CREATE TABLE `role` (
  `id` int(50) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'ADMIN'),
(2, 'MANAGER'),
(3, 'WAITER');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `role_action`
--

CREATE TABLE `role_action` (
  `id_role` int(50) NOT NULL,
  `id_action` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `role_action`
--

INSERT INTO `role_action` (`id_role`, `id_action`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `token` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `id_location` int(50) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `user`
--

INSERT INTO `user` (`id`, `username`, `token`, `email`, `firstName`, `lastName`, `id_location`, `active`) VALUES
(1, 'admin', 'e3274be5c857fb42ab72d786e281b4b8', 'admin@gmail.com', 'John', 'Anderson', 1, 1),
(2, 'manager', '099863f6cd95762fcd2ed7a79b283ec4', 'manager@gmail.com', 'Eva', 'Johnson', 1, 1),
(3, 'waiter', '8a60e0bb2ac6efde1d09e0ba5f1599f2', 'waiter@gmnail.com', 'Sam', 'Smith', 1, 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `user_role`
--

CREATE TABLE `user_role` (
  `id_user` int(50) NOT NULL,
  `id_role` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `user_role`
--

INSERT INTO `user_role` (`id_user`, `id_role`) VALUES
(1, 1),
(2, 2),
(3, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `actionCode` (`actionCode`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_location` (`id_location`);

--
-- Indexes for table `category_location`
--
ALTER TABLE `category_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_category_location` (`id_category_location`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_action`
--
ALTER TABLE `role_action`
  ADD PRIMARY KEY (`id_role`,`id_action`),
  ADD KEY `fk_id_role` (`id_role`),
  ADD KEY `fk_id_action` (`id_action`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `unique_index` (`firstName`,`lastName`),
  ADD KEY `fk_id_place` (`id_location`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id_user`,`id_role`),
  ADD KEY `fk_id_user` (`id_user`),
  ADD KEY `fk_id_role` (`id_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action`
--
ALTER TABLE `action`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `category_location`
--
ALTER TABLE `category_location`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restrictii pentru tabele sterse
--

--
-- Restrictii pentru tabele `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `fk_id_location` FOREIGN KEY (`id_location`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrictii pentru tabele `location`
--
ALTER TABLE `location`
  ADD CONSTRAINT `fk_id_category_location` FOREIGN KEY (`id_category_location`) REFERENCES `category_location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrictii pentru tabele `role_action`
--
ALTER TABLE `role_action`
  ADD CONSTRAINT `fk_id_action` FOREIGN KEY (`id_action`) REFERENCES `action` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_id_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrictii pentru tabele `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_id_place` FOREIGN KEY (`id_location`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrictii pentru tabele `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_id_roleee` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
